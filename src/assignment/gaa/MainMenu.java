package assignment.gaa;

public class MainMenu {
//sets up the options for the main menu and the switch statement chooses the method corresponding to the option chosen 
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int option;
		
		RegMenu reg = new RegMenu();
		do {
			option = reg.mainMenu();
			reg.readInFile();
			switch(option)
			{
			case 1 : reg.menuRegister();
			break;
			case 2 : reg.listPlayers();
			break;
			case 3 : reg.menuRemoveRegistration();
			break;
			case 4 : reg.sortPlayers();
			break;
			case 5 : reg.ratePlayers();
		    break;
			case 6 : reg.searchplayer();
			break;
			case 7 : reg.sortBySport();
			break;
			case 8 : reg.writeToFile();
			default:
		    break;
			}
		}
		while(option!=8);
	}

}
