package assignment.gaa.exc;
import java.io.Serializable;
import java.util.Comparator;

import assignment.gaa.AgeException;
import assignment.gaa.NameException;

import java.io.Serializable;
public abstract class GaaReg implements Serializable{//main abstract class that implements the file handling
	protected String name;
	protected String address;
	protected String gender;
	protected int age;
	
	public GaaReg(String name, String address, String gender, int age) 
	{
		super();
		this.name = name;
		this.address = address;
		this.gender = gender;
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) throws NameException {
		if (name.matches("[0-9]+"))
			throw new NameException();
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getAge() {
		return age;
	}
	
	public void setAge(int age) throws AgeException {
		if (age<0)
			throw new AgeException();
		this.age = age;
	}

	public void setRate(int valued) {
		// TODO Auto-generated method stub
		
	}
	
	
}
