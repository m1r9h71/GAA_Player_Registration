package assignment.gaa;
//class for position which declares position sport and rate


import assignment.gaa.exc.GaaReg;

public class Position extends GaaReg implements Comparable{
	String position; String sport; int rate; 
public Position (String name, String address, String gender, int age, int squadNumber, String position, String sport, int rate){
	super(name,address,gender,age);
	this.position = position;
	this.sport = sport;
	this.rate = rate;
}




public String getPosition() {
	return position;
}




public void setPosition(String position) {
	this.position = position;
}




public String getSport() {
	return sport;
}




public void setSport(String sport) {
	this.sport = sport;
}




public int getRate() {
	return rate;
}




public void setRate(int rate) {
	this.rate = rate;
}
public int sortByRating(){
	
	return rate;
}
public String sortByName(){
	return name;
}









@Override
public String toString() {
	return "Sport: " + sport + "    Position: " + position + "    Name: " + name + "     Address: " + address + "    Gender: " + gender
			+ "    Age: " + age + "    Rate: " + rate;
}




@Override
public int compareTo(Object arg0) {
	if (this.rate == ((Position)arg0).rate)
	return 0;
	else if ((this.rate) > ((Position)arg0).rate)
		return 1;
	else
		return -1;
}







}
