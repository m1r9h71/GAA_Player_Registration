package assignment.gaa;
@SuppressWarnings("serial")
public class NameException extends Exception{

	public NameException() {
		// TODO Auto-generated constructor stub
		super("Please enter a valid name.");
	}
	
	public NameException(String msg) {
		super(msg);
	}

}
