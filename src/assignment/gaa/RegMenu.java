package assignment.gaa;
import assignment.gaa.exc.*;
import fileHandler.FileHander;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

import javax.swing.JComboBox;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JSlider;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.JTextComponent;

public class RegMenu 
{ 
	//private static Icon myIcon = new ImageIcon("..\\Project Matt Hoing and  Matt Hendrick\\src\\images\\logo.jpg");
	private static Icon myIcon = new ImageIcon("..\\Project Matt Hoing and  Matt Hendrick\\src\\images\\gaalogo.jpg");
	private static Icon anIcon = new ImageIcon();
	private static ArrayList<Position> registerList = new ArrayList<Position>(); //creating array list
	FileHander FileHandler = new FileHander();
	
public RegMenu()
{//sets up the first player in the arrayList
	Position aPlayer = new Position("Joe Bloggs", "Tombrack, Ferns, Co. Wexford", "Male", 24, 1 ,"Goal", "Hurling",4);
	registerList.add(aPlayer);

	try {
		registerList = FileHandler.readInFile();//reads in existing arrayList
	} catch (FileNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (ClassNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

}
	
public int mainMenu() 
{
	int option = 0;
	//options linked to the MainMenu class
	String option1 = new String("1. Register player");
	String option2 = new String("2. View registered players");
	String option3 = new String("3. Remove Registered Players");
	String option4 = new String("4. Sort Teams");
	String option5 = new String("5. Rate Players: ");
	String option6 = new String("6. Search for players: ");
	String option7 = new String("7. Filter by sport: ");
	String option8 = new String("8. Exit");		
	JTextField opt = new JTextField("");
	
	 Object message[] = new Object[10];
	 
	 message[0] = myIcon;//image
	 message[1] = option1;
	 message[2] = option2;
	 message[3] = option3;
	 message[4] = option4;
	 message[5] = option5;
	 message[6] = option6;
	 message[7] = option7;
	 message[8] = option8;
	 message[9] = opt;
	 
	 int response = JOptionPane.showConfirmDialog(null,message,"Enter Player Details",JOptionPane.OK_CANCEL_OPTION,
	                                              JOptionPane.QUESTION_MESSAGE ,anIcon);
	 
	 if (response == JOptionPane.CANCEL_OPTION);
	 else if(response == JOptionPane.CANCEL_OPTION)
	 {
		 System.exit(0);
	 }
	 else
	 {
		 try {
			 option = Integer.parseInt( opt.getText());
		 }
		 catch (Exception exc)
		 {
			 JOptionPane.showMessageDialog(null, "There has been an error" + exc + " Try again please");
		 }
	 }
	 return option;
}

public void menuRegister()
{//adds player object to arrayList
	String msgName = new String("Player Name: ");
	String msgAddress = new String("Address: ");
	
	String msgGender = new String("Gender: ");
	String[] genders = {"Male", "Female"};
	
	String msgAge = new String("Age: ");
	String msgNumber = new String("Squad Number: ");
	String [] numbers = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15"};
	String msgRate = new String ("Player Rating: ");
	String [] ratings = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
	JTextField name = new JTextField("");
	JTextField address = new JTextField("");
	JComboBox gender = new JComboBox(genders);
	JTextField age = new JTextField("");
	JComboBox squad = new JComboBox(numbers); 
	JComboBox rate = new JComboBox(ratings);
	Object message[] = new Object[13];
	
	message[0] = myIcon;
	message[1] = msgName;
	message[2] = name;
	message[3] = msgAddress;
	message[4] = address;
	message[5] = msgGender;
	message[6] = gender;
	message[7] = msgAge;
	message[8] = age;
	message[9] = msgNumber;
	message[10] = squad;
	message[11] = msgRate;
	message[12] = rate;
	 int response = JOptionPane.showConfirmDialog(null,message,"Player Details",JOptionPane.OK_CANCEL_OPTION,
                                                   JOptionPane.QUESTION_MESSAGE ,anIcon);

	 if(response == JOptionPane.CANCEL_OPTION);
	 
	 else 
	 {
		 
		 try {
			 String Name = name.getText();
			 if (Name.matches("[0-9]+")){
				 throw new NameException();
			 }
			 
			 String Address = address.getText();
			 String Gender = gender.getSelectedItem().toString(); 
			 int Age = Integer.parseInt(age.getText());
			 int Number = Integer.parseInt(squad.getSelectedItem().toString());
			 int Rated = Integer.parseInt(rate.getSelectedItem().toString()); 
			 if(Age < 0){
				 throw new AgeException();
			 }
			 	else { 
				
			 if (Number == 1 && Gender == "Male"){
				 String Goal = new String ("Goalkeeper");
				 String ms1 = new String ("Hurling");
				 Position pos1 = new Position (Name, Address, Gender, Age, Number, Goal, ms1, Rated);
				 registerList.add(pos1);
			 }
			 else if (Number == 2 && Gender == "Male" || Number == 4 && Gender == "Male"){
				 String cornerBack = new String ("Corner Back"); 
				 String ms2 = new String ("Hurling");
						Position pos2 = new Position (Name, Address, Gender, Age, Number, cornerBack, ms2, Rated);
						registerList.add(pos2);
			 }
			 
			 else if (Number == 3 && Gender == "Male") {
				 String fullBack = new String ("FullBack");
				 String ms3 = new String ("Hurling");
				 	Position pos3 = new Position (Name, Address, Gender, Age, Number, fullBack, ms3, Rated);
					registerList.add(pos3);
			 }
			 
			 else if (Number == 5 && Gender == "Male" || Number == 7 && Gender == "Male" ){
				 String wingBack = new String ("Wing Back");
				 String ms4 = new String ("Hurling");
				 Position pos4 = new Position (Name, Address, Gender, Age, Number, wingBack, ms4, Rated);
				 registerList.add(pos4);
			 } 
			 
			 else if (Number == 6 && Gender == "Male") {
				 String centerBack = new String ("Center Back");
				 String ms5 = new String ("Hurling");
				 	Position pos5 = new Position (Name, Address, Gender, Age, Number, centerBack, ms5, Rated);
					registerList.add(pos5);
			 }
			 
			 
			 else if (Number == 8 && Gender == "Male" || Number == 9 && Gender == "Male"){
				 String midfield = new String ("Midfield");
				 String ms6 = new String ("Hurling");
				 Position pos6  = new Position (Name, Address, Gender, Age, Number, midfield, ms6, Rated);
				 registerList.add(pos6);
			 }
			 else if (Number == 10 && Gender == "Male" || Number == 12&& Gender == "Male" ){
				 String wingForward = new String ("Wing Forward");
				 String ms7 = new String ("Hurling");
				 Position pos7 = new Position (Name, Address, Gender, Age, Number,wingForward, ms7, Rated);
				 registerList.add(pos7);
			 }
			 
			 else if (Number == 11 && Gender == "Male") {
				 String centerForward = new String ("Center Forward");
				 String ms8 = new String ("Hurling");
				 	Position pos8 = new Position (Name, Address, Gender, Age, Number, centerForward, ms8, Rated);
					registerList.add(pos8);
			 }
			 
			 
			 else if(Number == 13 && Gender == "Male" || Number == 15&& Gender == "Male"){
				 String cornerForward = new String ("Corner Forward");
				 String ms9 = new String ("Hurling");
				 Position pos9 = new Position (Name, Address, Gender, Age, Number, cornerForward, ms9, Rated);
				 registerList.add(pos9);
			 }
			 
			 else if (Number == 14 && Gender == "Male") {
				 String fullForward = new String ("Full Forward");
				 String ms10 = new String ("Hurling");
				 	Position pos10 = new Position (Name, Address, Gender, Age, Number, fullForward, ms10, Rated);
					registerList.add(pos10);
			 }
			 
			 
			 else if (Number == 1 && Gender == "Female"){
				 String Goal2 = new String ("Goalkeeper");
				 String fs1 = new String ("Camogie");
				 Position pos11 = new Position (Name, Address, Gender, Age,Number,Goal2, fs1, Rated);
				 registerList.add(pos11);
			 }
			 else if (Number == 2 && Gender == "Female" || Number == 4 && Gender == "Female"){
				 String cornerBack2 = new String ("Corner Back"); 
				 String fs2 = new String ("Camogie");
						Position pos12 = new Position (Name, Address, Gender, Age, Number, cornerBack2, fs2, Rated);
						registerList.add(pos12);
			 }
			 
			 else if (Number == 3 && Gender == "Female") {
				 String fullBack2 = new String ("Full Back"); 
				 String fs3 = new String ("Camogie");
						Position pos13 = new Position (Name, Address, Gender, Age, Number, fullBack2, fs3, Rated);
						registerList.add(pos13);
			 }
			 
			 else if (Number == 5 && Gender == "Female" || Number == 7 && Gender == "Female" ){
				 String wingBack2 = new String ("Wing Back");
				 String fs4 = new String ("Camogie");
				 Position pos14 = new Position (Name, Address, Gender, Age, Number, wingBack2, fs4, Rated);
				 registerList.add(pos14);
			 }
			 
			 else if (Number ==6 && Gender == "Female") {
				 String centerBack2 = new String ("Center Back");
			     String fs5 = new String ("Camogie");
					Position pos15 = new Position (Name, Address, Gender, Age, Number, centerBack2, fs5, Rated);
					registerList.add(pos15);
			 }
			 
			 else if (Number == 8 && Gender == "Female" || Number == 9 && Gender == "Female"){
				 String midfield2 = new String ("Midfield");
				 String fs6 = new String ("Camogie");
				 Position pos16  = new Position (Name, Address, Gender, Age, Number, midfield2, fs6, Rated);
				 registerList.add(pos16);
			 }
			 else if (Number == 10 && Gender == "Female" || Number == 12&& Gender == "Female" ){
				 String wingForward2 = new String ("Wing Forward");
				 String fs7 = new String ("Camogie");
				 Position pos17 = new Position (Name, Address, Gender, Age, Number,wingForward2, fs7, Rated);
				 registerList.add(pos17);
			 }
			 
			 else if (Number == 11 && Gender == "Female") {
				 String centerForward2 = new String ("Center Forward");
			     String fs8 = new String ("Camogie");
					Position pos18 = new Position (Name, Address, Gender, Age, Number, centerForward2, fs8, Rated);
					registerList.add(pos18);
			 }
			 
			 
			 else if(Number == 13 && Gender == "Female" || Number == 15&& Gender == "Female"){
				 String cornerForward2 = new String ("Corner Forward");
				 String fs9 = new String ("Camogie");
				 Position pos19 = new Position (Name, Address, Gender, Age, Number, cornerForward2, fs9, Rated);
				 registerList.add(pos19);
			 }
			 
			 else if (Number == 14 && Gender == "Female") {
				 String fullForward2 = new String ("FullForward");
			     String fs10 = new String ("Camogie");
					Position pos20 = new Position (Name, Address, Gender, Age, Number, fullForward2, fs10, Rated);
					registerList.add(pos20);
			 }
			 
			 
			 else{
				 JOptionPane.showMessageDialog(null, "There has been an error. Try again please");
			 }
			 
		 }
		 }
			 
			 	
			 
			 
		 
	 

	 catch (AgeException a)
	 {
		 JOptionPane.showMessageDialog(null,a.getMessage());
	 }
		 
	catch (Exception exc)
	 {			 
		JOptionPane.showMessageDialog(null, "There has been an error: " + exc );
	 }
	 
}

}
/////////////////////////////////////////////////////////////////////////////////////
public void listPlayers()
{//lists the arrayList on the GUI
	Object message[] = new Object[4];
	
	message[0] = myIcon;
	message[1] = "";
	message[2] = registerList.toArray();
	
	JTable table = new JTable();
	DefaultTableModel model = new DefaultTableModel();
	table.setModel(model);
	model.setColumnIdentifiers(new String[] {"Sport", "Position", "Name", "Address", "Gender", "Age", "Rate"});
	
	for (int i=0; i<registerList.size();i++)
	{
		model.addRow(message);
	   
	}
	 JOptionPane.showMessageDialog(null, message);
	
}
////////////////////////////////////////////////////////////////////////////////////
public void menuRemoveRegistration()
{	
	//shows the player objects and deletes the one chosen
	String msgName = new String("Player Name: ");
	
	
	JTextField name = new JTextField("");
	
	JComboBox players = new JComboBox(registerList.toArray());
	Object message[] = new Object[3];
	
	
	message[0] = myIcon;
	
	message[1] = msgName;
	message[2] = players;
	
	int response = JOptionPane.showConfirmDialog(null, message, "Player Date Entry: REMOVE Player", JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, anIcon);
	
	if(response == JOptionPane.CANCEL_OPTION){}
	else
	{
	try{
		
		String tempName = players.getSelectedItem().toString();
		
		
		
		for(int i=0; i<registerList.size();i++){
			if(registerList.get(i).toString().contains(tempName))
			{
					registerList.remove(i);
					break;

			}

		}
	}
	
	
	catch (Exception e)
	{
		JOptionPane.showMessageDialog(null, "Data Input Error" + e + "\nPlease Try Again");
	}
	}
	}
/////////////////////////////////////////////////////////////////////////////////////
public void sortPlayers(){
	//sorts the arrayList in ascending order by rate	
	Collections.sort(registerList);
	Iterator itr = registerList.iterator();
	while (itr.hasNext()){
		Object element = itr.next();
		System.out.println(element + "\n");
		
		
		
		
		//JOptionPane.showMessageDialog(null, element);
	}
/*String msgTeam = new String ("Pick from Match or Training Teams");
String [] teams = {"Match", "Training"};
JComboBox teamPick = new JComboBox (teams);
Object message [] = new Object[4];

message [0] = myIcon;
message [1] = msgTeam;
message [2] = teams;
message [3] = teamPick;

int response = JOptionPane.showConfirmDialog(null,message,"Player Details",JOptionPane.OK_CANCEL_OPTION,
        JOptionPane.QUESTION_MESSAGE ,anIcon);

if(response == JOptionPane.CANCEL_OPTION);

else 
{

try {
	
	
	 String teamChoice = teamPick.getSelectedItem().toString(); 
	 for(int i=0; i<registerList.size();i++){
		 
			//{Collections.sort(registerList<>);
	 
	//investigate the sort function - we need to sort by name, sport, position 
//Collections.sort(registerList<Position> );
//for (Position rate: registerList)
	//System.out.print(rate.sortByRate() + ", ");

//Collections.sort(registerList, new Position());
//System.out.println(" ");
//for (Position rate : registerList)
//	System.out.print(rate.sortByRate() +" , ");


//Collections.sort(registerList, ());
//System.out.println(" ");
//for (Position rate : registerList)
//	System.out.println(rate.sortByRate() + " : " + rate.sortByName() + " , ");
}
}
catch (Exception exc)
{			 
	JOptionPane.showMessageDialog(null, "There has been an error" + exc + " Try again please");
}
}*/
	
}
/////////////////////////////////////////////////////////////////////////////////////
public void ratePlayers(){
	//puts a slider into the GUI changes the rating of a player object
	String msgSlider = new String ("Change Player Rating: ");
	JSlider slider = new JSlider(JSlider.HORIZONTAL, 0, 10, 0);
	JComboBox players2 = new JComboBox(registerList.toArray());
	
	slider.setMajorTickSpacing(10);
	slider.setMinorTickSpacing(1);
	slider.setPaintTicks(true);
	slider.setPaintLabels(true);
	
	//JTextField opt = new JTextField("");

	Object message [] = new Object[4];

	message [0] = myIcon;
	message [1] = msgSlider;
	message [2] = slider;
	message [3] = players2;
	//message [4] = opt;
	int response = JOptionPane.showConfirmDialog(null,message,"Player Details",JOptionPane.OK_CANCEL_OPTION,
	        JOptionPane.QUESTION_MESSAGE ,anIcon);

	if(response == JOptionPane.CANCEL_OPTION);

	else 
	{

	try {
		int valued = slider.getValue();
		
		 String teamChoice = players2.getSelectedItem().toString(); 
		//investigate the sort function - we need to sort by name, sport, position
		 for(int i = 0; i < registerList.size();i ++){
			if (registerList.get(i).toString().contains(teamChoice)){


	
			 registerList.get(i).setRate(valued);
			 break;
			}
				
			
			
		 }
	}
	catch (Exception exc)
	{			 
		JOptionPane.showMessageDialog(null, "There has been an error" + exc + " Try again please");
	}
	}
	 
	 
}
//////////////////////////////////////////////////////////////////////////////
public void searchplayer(){
	String msgName = new String("Player Name: ");
	
	JTextField name = new JTextField("");
Object message[] = new Object[3];
	
	message[0] = myIcon;
	message[1] = msgName;
	message[2] = name;
	int response = JOptionPane.showConfirmDialog(null,message,"Player Details",JOptionPane.OK_CANCEL_OPTION,
            JOptionPane.QUESTION_MESSAGE ,anIcon);

if(response == JOptionPane.CANCEL_OPTION);

else 
{

try {
	 String Name = name.getText();
	 
		 
		 for(int i=0; i<registerList.size();i++){
				if(registerList.get(i).toString().contains(Name)){
					
					JOptionPane.showMessageDialog(null, registerList.get(i));
					
				}
				
				}
	 
	 }
	

	catch (Exception exc)
	 {			 
		JOptionPane.showMessageDialog(null, "There has been an error: " + exc );
	 }
	 
}

}
//////////////////////////////////////////////////////////////////////////////
public void sortBySport(){
String msgSport = new String("Sport Name: ");
	
	JTextField sport = new JTextField("");
Object message[] = new Object[3];
	
	message[0] = myIcon;
	message[1] = msgSport;
	message[2] = sport;
	int response = JOptionPane.showConfirmDialog(null,message,"Player Details",JOptionPane.OK_CANCEL_OPTION,
            JOptionPane.QUESTION_MESSAGE ,anIcon);

if(response == JOptionPane.CANCEL_OPTION);

else 
{
	try {
		 String Sport = sport.getText();
		 
			 
			 for(int i=0; i<registerList.size();i++){
					if(registerList.get(i).toString().contains(Sport)){
						
						JOptionPane.showMessageDialog(null, registerList.get(i));
						
					}
					
					}
		 
	}catch (Exception exc)
	 {			 
		JOptionPane.showMessageDialog(null, "There has been an error: " + exc );
	 }
	}	 
}


/////////////////////////////////////////////////////////////////////////////////////
public void writeToFile(){ //writes to file declared at top of the class
	try {
		FileHandler.writeToFile(registerList, registerList.size());
	} catch (FileNotFoundException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	}
}

public void readInFile(){ //reads into file declared at the top of the class
	try {
		FileHandler.readInFile();
	} catch (FileNotFoundException e) {
		e.printStackTrace();
	} catch (ClassNotFoundException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	}
}
}
