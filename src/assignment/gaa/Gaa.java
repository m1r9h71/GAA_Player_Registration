package assignment.gaa;
import assignment.gaa.exc.*;
// class that declare the squadNumber and getRate 
public class Gaa extends GaaReg {
	int squadNumber;
	public int getRate;
	public Gaa(String name, String address, String gender, int age, int squadNumber)
	{
		super(name,address,gender,age);
		this.squadNumber = squadNumber;
	}
	
	@Override
	public String toString() {
		return "Player Information: Name: " + name + ", Address: " + address 
				+ ", Gender: " + gender + ", Age: " + age + ", Squad Number: " + squadNumber + "\n";
	}

}
