package assignment.gaa;
@SuppressWarnings("serial")//if wrong age is entered the message pops up
public class AgeException extends Exception {

	public AgeException() {
		super ("Please enter a valid age");
	}
	
	public AgeException(String msg) {
		super(msg);
	}
}
