package fileHandler;




import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import assignment.gaa.Position;
import assignment.gaa.exc.GaaReg;
public class FileHander{
	
	File myFile = new File("stock.dat");//declares file and location
	ObjectOutputStream oos;
	ObjectInputStream ois;
	int size = 0;
	//write the ArrayList to the file 
	public void writeToFile(ArrayList<Position> devices,int size) throws FileNotFoundException, IOException{
		oos = new ObjectOutputStream(new FileOutputStream(myFile));
		oos.writeInt(size);
		oos.writeObject(devices);
		oos.close();
	}
	//read in from file
	@SuppressWarnings("unchecked")
	public ArrayList<Position> readInFile() throws FileNotFoundException, IOException, ClassNotFoundException {
		
		ArrayList<Position> temp = null;
		ois = new ObjectInputStream(new FileInputStream(myFile));
		size = ois.readInt();
		temp = (ArrayList<Position>) ois.readObject();
		ois.close();
		return temp;
	}
}
